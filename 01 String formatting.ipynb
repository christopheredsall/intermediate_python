{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String formatting\n",
    "\n",
    "Being able to print things to the screen is a fundamental part of interacting with a programming language. So far, we've kept it simple by passing the data we want to print directly to the `print` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(my_num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We made things a bit nicer by using the fact that if you pass multiple arguments to `print` then it will print each of them, separated by spaces, allowing us to combine our data with a message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My num is 42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(\"My num is\", my_num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works perfectly well but firstly you're using the implicit space that's added as part of your sentence and secondly you're passing in two separate pieces of information where they are logically one.\n",
    "\n",
    "What we can do instead is create a single string which contains the message we want to print and put inside it special placeholders for where we want our data to appear. There's a few different ways to do this in Python (an older but still valid method you may see uses `%`) but for this course we will use the method that uses `{}`.\n",
    "\n",
    "If you write a string that contains a `{}` and then call the `format` method on that string then it will replace each `{}` in turn with each argument to `format`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My num is 42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(\"My num is {}\".format(my_num))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is not specific to the `print` function, you can use this template formatting anywhere. For example, we can split up the template creation and the application of the placeholder replacement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My num is 42\n"
     ]
    }
   ],
   "source": [
    "template = \"My num is {}\"\n",
    "\n",
    "my_string = template.format(my_num)\n",
    "\n",
    "print(my_string)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you have multiple placeholders then all the `{}` can get confusing so you can also give each placeholder a name. If you put a word inside the `{}` then you can refer to it in the call to `format` and it will replace by name rather than argument position:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The answer is 42 and the circle constant is 3.14159\n"
     ]
    }
   ],
   "source": [
    "answer = 42\n",
    "pi = 3.14159\n",
    "\n",
    "print(\"The answer is {the_answer} and the circle constant is {circle}\".format(circle=pi, the_answer=answer))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are using a newer version of Python, i.e. version 3.6 (released december 2016) or newer then you can make use of an easier formatting method called *f-strings*. This allows you to skip the `format` call and refer directly to variable names inside the string. You use it by putting an `f` before the opening quotation mark and then put the variable you want to use directly inside the `{}`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The answer is 42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(f\"The answer is {my_num}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, to change the previous example to use f-strings, it would look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The answer is 42 and the circle constant is 3.14159\n"
     ]
    }
   ],
   "source": [
    "answer = 42\n",
    "pi = 3.14159\n",
    "\n",
    "print(f\"The answer is {answer} and the circle constant is {pi}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "For all the exercises in this course, we are going to be working on some Python code which converts to and from [Morse Code](https://simple.wikipedia.org/wiki/Morse_code). In each section we will add or change the code so make sure that you don't skip any exercises.\n",
    "\n",
    "- Copy the following code in the text editor into a script called `encode.py` and run it in the terminal with `python encode.py` to check that it works.\n",
    "\n",
    "  ```python\n",
    "  letter_to_morse = {'a':'.-', 'b':'-...', 'c':'-.-.', 'd':'-..', 'e':'.', 'f':'..-.', \n",
    "                     'g':'--.', 'h':'....', 'i':'..', 'j':'.---', 'k':'-.-', 'l':'.-..', 'm':'--', \n",
    "                     'n':'-.', 'o':'---', 'p':'.--.', 'q':'--.-', 'r':'.-.', 's':'...', 't':'-',\n",
    "                     'u':'..-', 'v':'...-', 'w':'.--', 'x':'-..-', 'y':'-.--', 'z':'--..',\n",
    "                     '0':'-----', '1':'.----', '2':'..---', '3':'...--', '4':'....-',\n",
    "                     '5':'.....', '6':'-....', '7':'--...', '8':'---..', '9':'----.',\n",
    "                     ' ':'/'}\n",
    "\n",
    "  message = \"SOS We have hit an iceberg and need help quickly\"\n",
    "\n",
    "  morse = []\n",
    "\n",
    "  for letter in message:\n",
    "      letter = letter.lower()\n",
    "      morse.append(letter_to_morse[letter])\n",
    "\n",
    "  # We need to join together Morse code letters with spaces\n",
    "  morse_message = \" \".join(morse)\n",
    "  \n",
    "  print(f\"Incoming message: {message}\")\n",
    "  print(f\"   Morse encoded: {morse_message}\")\n",
    "  ```\n",
    "- [<small>answer</small>](answer_morse_string_formatting.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
