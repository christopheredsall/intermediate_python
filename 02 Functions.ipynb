{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Functions\n",
    "\n",
    "In the exercise at the end of the last chapter, you edited a script that could encode messages to Morse code. The script is good, but is not very easy to use or reusable. For someone to make use of the script, they will have to edit it and copy and paste your code every time they want to encode a message.\n",
    "\n",
    "Functions provide a way of packaging code into reusable and easy-to-use components. To explain how they work, lets imagine we have some code to add together two arrays. Open a new IPython Console session and type the below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6, 8, 10, 12]\n"
     ]
    }
   ],
   "source": [
    "a = [1, 2, 3, 4]\n",
    "b = [5, 6, 7, 8]\n",
    "\n",
    "c = []\n",
    "for a_elem, b_elem in zip(a, b):\n",
    "    c.append(a_elem + b_elem)\n",
    "\n",
    "print(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This code has added the arrays `a` and `b` together to make `c`. It used the `zip` function to combine two lists into a single list of pairs which we then iterate over.\n",
    "\n",
    "You can see this script as having three main parts to it:\n",
    "- The set up where we define `a` and `b`\n",
    "- The data-processing section where we read our inputs and create an output\n",
    "- The output section where we print our result to the screen\n",
    "\n",
    "The data processing section will work regardless of what data is inside the lists `a` and `b` and so we can grab that bit of code and make it usable in other contexts quite easily, using functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining functions\n",
    "\n",
    "We can turn this into a function that can add any two arrays together by using `def`. To do this, type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "def add_arrays(x, y):\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has created a new function called `add_arrays` which we can now call. In a similar fashion to other constructs in Python (like `for` loops and `if` statements) it has a rigid structure.\n",
    "\n",
    "First we must use the `def` keyword to start a function definition:\n",
    "\n",
    "<pre>\n",
    " ↓\n",
    "<b>def</b> add_arrays(x, y):\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z\n",
    "</pre>\n",
    "\n",
    "Then we specify the name that we want to give the function. Like anything in Python, choose a descriptive name that describes what it does. This is the name which we will use when *calling* the function:\n",
    "\n",
    "<pre>\n",
    "        ↓\n",
    "def <b>add_arrays</b>(x, y):\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z\n",
    "</pre>\n",
    "\n",
    "Function definitions must then be followed by a pair of round brackets. This is a similar syntax to that used when *calling* a function and giving it arguments but here we're just defining it:\n",
    "\n",
    "<pre>\n",
    "              ↓    ↓\n",
    "def add_arrays<b>(</b>x, y<b>)</b>:\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z\n",
    "</pre>\n",
    "\n",
    "Between those brackets go the names of the parameters we want the function to accept. We can define zero or more parameters. Here we are defining two:\n",
    "\n",
    "<pre>\n",
    "               ↓  ↓\n",
    "def add_arrays(<b>x, y</b>):\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z\n",
    "</pre>\n",
    "\n",
    "Finally, the line is completed with a colon:\n",
    "\n",
    "<pre>\n",
    "                    ↓\n",
    "def add_arrays(x, y)<b>:</b>\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z\n",
    "</pre>\n",
    "\n",
    "Since we've used a colon, we must indent the body of the function as we did with loops and conditional statements:\n",
    "\n",
    "<pre>\n",
    "def add_arrays(x, y):\n",
    "    <b>z = []\n",
    "    for x_elem, y_elem in zip(x, y):</b>  ← body of function<b>\n",
    "        z.append(x_elem + y_elem)\n",
    "    return z</b>\n",
    "</pre>\n",
    "\n",
    "Most function will also want to return data back to the code that called it. You can choose what data is returned using the `return` keyword followed by the data you want to return:\n",
    "\n",
    "<pre>\n",
    "def add_arrays(x, y):\n",
    "    z = []\n",
    "    for x_elem, y_elem in zip(x, y):\n",
    "        z.append(x_elem + y_elem)\n",
    "    <b>return</b> z\n",
    "      ↑\n",
    "</pre>\n",
    "\n",
    "The body of the function has been copied from our script above with the only change being that the variables have different names. Our input data lists are still called `a` and `b` but inside the function we will refer to any arguments passed as `x` and `y` with the output being called `z`.\n",
    "\n",
    "## Calling functions\n",
    "\n",
    "You can now add the arrays together by calling the function using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6, 8, 10, 12]\n"
     ]
    }
   ],
   "source": [
    "d = add_arrays(a, b)\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case you have called the function `add_arrays` and passed in the arguments `a` and `b`. `a` is copied to `x`, while `b` is copied to `y`. The function `add_arrays` then acts on `x` and `y`, creating the summed array `z`. It then returns the new array `z`, which is copied back to `d`.\n",
    "\n",
    "You can use your new `add_arrays` function to add together other arrays. Try typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[5.1, 12.2, 8.3]\n"
     ]
    }
   ],
   "source": [
    "r = [0.1, 0.2, 0.3]\n",
    "s = [5, 12, 8]\n",
    "t = add_arrays(r, s)\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should see that `t` is the array `[5.1, 12.2, 8.3]`.\n",
    "\n",
    "Note that we can pass the values to the function directly, e.g. type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[6, 8, 10]\n"
     ]
    }
   ],
   "source": [
    "r = add_arrays([1, 2, 3], [5, 6, 7])\n",
    "print(r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This should show you that `r` is equal to the array `[6, 8, 10]`.\n",
    "\n",
    "Note that you must pass in the right number of arguments to a function. `add_arrays` expects two arguments, so if you pass more or less, then that is an error. Try this now:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "add_arrays() missing 2 required positional arguments: 'x' and 'y'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-6-461b2fd323ee>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mr\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0madd_arrays\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: add_arrays() missing 2 required positional arguments: 'x' and 'y'"
     ]
    }
   ],
   "source": [
    "r = add_arrays()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "so as you can see, it tells you that you've given it the wrong number of arguments. It expects 2 (`x` and `y`). Likewise, if you give too many arguments you get a similar error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "add_arrays() takes 2 positional arguments but 3 were given",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-7-d8c09abe4a14>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mr\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0madd_arrays\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0ma\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mb\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0mc\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: add_arrays() takes 2 positional arguments but 3 were given"
     ]
    }
   ],
   "source": [
    "r = add_arrays(a, b, c)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note also that you can define your function to take as many arguments, and return as many values as you want, e.g. try typing:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3\n",
      "7\n",
      "5\n"
     ]
    }
   ],
   "source": [
    "def lotsOfArgs(a, b, c, d, e):\n",
    "    return a + b, c + d, e\n",
    "\n",
    "r, s, t = lotsOfArgs(1, 2, 3, 4, 5)\n",
    "print(r)\n",
    "print(s)\n",
    "print(t)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can you work out why we get the output that we see above?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Take `encode.py` from the previous chapter and edit it so that the part that does the conversion (everything from `morse = []` to `\" \".join(morse)`) is moved into a function. The function should take one argument, `message` and return the encoded morse string. Choose a sensible, one word name for the function. [<small>answer</small>](answer_morse_encode_function.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Make a second file called `decode.py` with the contents:\n",
    "  ```python\n",
    "  letter_to_morse = {'a':'.-', 'b':'-...', 'c':'-.-.', 'd':'-..', 'e':'.', 'f':'..-.', \n",
    "                     'g':'--.', 'h':'....', 'i':'..', 'j':'.---', 'k':'-.-', 'l':'.-..', 'm':'--', \n",
    "                     'n':'-.', 'o':'---', 'p':'.--.', 'q':'--.-', 'r':'.-.', 's':'...', 't':'-',\n",
    "                     'u':'..-', 'v':'...-', 'w':'.--', 'x':'-..-', 'y':'-.--', 'z':'--..',\n",
    "                     '0':'-----', '1':'.----', '2':'..---', '3':'...--', '4':'....-',\n",
    "                     '5':'.....', '6':'-....', '7':'--...', '8':'---..', '9':'----.',\n",
    "                     ' ':'/' }\n",
    "\n",
    "  # We need to invert the dictionary. This will create a dictionary\n",
    "  # that can go from the morse back to the letter\n",
    "  morse_to_letter = {}\n",
    "  for letter in letter_to_morse:\n",
    "      morse = letter_to_morse[letter]\n",
    "      morse_to_letter[morse] = letter\n",
    "\n",
    "  message = \"... --- ... / .-- . / .... .- ...- . / .... .. - / .- -. / .. -.-. . -... . .-. --. / .- -. -.. / -. . . -.. / .... . .-.. .--. / --.- ..- .. -.-. -.- .-.. -.--\"\n",
    "\n",
    "  english = []\n",
    "\n",
    "  # Now we cannot read by letter. We know that morse letters are\n",
    "  # separated by a space, so we split the morse string by spaces\n",
    "  morse_letters = message.split(\" \")\n",
    "\n",
    "  for letter in morse_letters:\n",
    "      english.append(morse_to_letter[letter])\n",
    "\n",
    "  # Rejoin, but now we don't need to add any spaces\n",
    "  english_message = \"\".join(english)\n",
    "  \n",
    "  print(english_message)\n",
    "  ```\n",
    "- Edit `decode.py` so that the part that does the conversion (everything from `english = []` to `\"\".join(english)`) is moved into a function. The function should take one argument, `message` and return the decoded english message. Choose a sensible, one word name for the function. [<small>answer</small>](answer_morse_decode_function.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
